#!/usr/bin/env clisp

(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp" (user-homedir-pathname))))
	(when (probe-file quicklisp-init)
		(load quicklisp-init)))
(ql:quickload "zpng")
(ql:quickload "cl-utilities")
(ql:quickload "uiop")

(defpackage #:tiedye (:use #:zpng #:cl #:cl-utilities #:uiop))
(in-package #:tiedye)

(defvar loop-step 1)
(defvar start 1)
(defvar wid 400)
(defvar hgt 400)

(defvar output ".")
(defvar maxr 255)
(defvar maxg 255)
(defvar maxb 255)

(defun draw-rgb (file)
  (format t "Writing to '~A'" file)
  (let ((png (make-instance 'pixel-streamed-png
                             :color-type :truecolor-alpha
                             :width wid
                             :height hgt)))
    (with-open-file (stream file
			    :direction :output
			    :if-exists :supersede
			    :if-does-not-exist :create
			    :element-type '(unsigned-byte 8))
      (start-png png stream)
      (loop for x from start to wid by loop-step
	do (loop for y from start to hgt by loop-step
		 do (write-pixel (list (random maxr) (random maxg) (random maxb) 255) png)))
      (finish-png png))))

(defun read-file (file)
  (with-open-file (stream file :direction :input)
    (loop for line = (read-line stream nil)
      while line
        collect line)))

(defun parse-config (config)
  (loop for line in config
    do (let ((splt (uiop:split-string line :separator "=")))
      (if (string= "output-dir" (nth 0 splt))
        (setq output (nth 1 splt)))
      (if (string= "max-red" (nth 0 splt))
        (setq maxr (parse-integer (nth 1 splt))))
      (if (string= "max-green" (nth 0 splt))
	(setq maxg (parse-integer (nth 1 splt))))
      (if (string= "max-blue" (nth 0 splt))
	(setq maxb (parse-integer (nth 1 splt))))
      (if (string= "width" (nth 0 splt))
	(setq wid (parse-integer (nth 1 splt))))
      (if (string= "height" (nth 0 splt))
	(setq hgt (parse-integer (nth 1 splt))))
      )))

(defun write-config ()
  (format t "Output : ~A~%Max Red : ~D~%Max Green : ~D~%Max Blue : ~D~%Width : ~D~%Height : ~D~%" output maxr maxg maxb wid hgt))

(parse-config (read-file (merge-pathnames ".config/tiedye.conf" (user-homedir-pathname))))
(write-config)
(draw-rgb (merge-pathnames "tiedye.png" output))
